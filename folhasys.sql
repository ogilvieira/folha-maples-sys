-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 15-Nov-2016 às 02:29
-- Versão do servidor: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `folhasys`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `allocation`
--

CREATE TABLE `allocation` (
  `id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `target_type` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `start` int(100) NOT NULL,
  `end` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `checkin`
--

CREATE TABLE `checkin` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `start` int(100) NOT NULL,
  `end` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `create_in` int(100) NOT NULL,
  `hs` int(100) DEFAULT '0',
  `date_plan` int(100) DEFAULT '0',
  `monthly` int(1) NOT NULL DEFAULT '0',
  `birth` int(3) NOT NULL DEFAULT '5'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `customers`
--

INSERT INTO `customers` (`id`, `name`, `create_in`, `hs`, `date_plan`, `monthly`, `birth`) VALUES
(1, 'Maples', 1460068098, 0, 0, 0, 5),
(2, 'Emannuelle Junqueira', 1460069038, 0, 0, 0, 5),
(3, 'Chama Supermercados', 1460069066, 0, 0, 0, 5),
(4, 'Auticomp', 1460069071, 100, 1483149600, 1, 15),
(5, 'Difordek', 1460069082, 0, 0, 0, 5),
(6, 'CRE/Arius', 1460069105, 0, 0, 0, 5),
(7, 'Ecoforros', 1460069129, 0, 0, 0, 5),
(8, 'Barbosa Supermercados', 1460069146, 0, 1474254000, 0, 5),
(9, 'João e Maria Barbearia', 1460069156, 0, 0, 0, 5),
(10, 'Papagaio Supermercados', 1460069283, 0, 0, 0, 5),
(11, 'Baby Brink', 1460069297, 10, 1474167600, 1, 5),
(12, 'Farma Chama', 1460069302, 0, 0, 0, 5),
(13, 'SM Services', 1472086210, 0, 0, 0, 5),
(14, 'Estúdio Campo', 1472086229, 0, 0, 0, 5),
(15, 'Marçalo Supermercados', 1472086765, 0, 0, 0, 5),
(16, 'Bebê Magazine', 1472087221, 0, 0, 0, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `passwords`
--

CREATE TABLE `passwords` (
  `id` int(10) NOT NULL,
  `id_customer` int(1) NOT NULL DEFAULT '1',
  `title` text,
  `pass_type` int(3) DEFAULT '1',
  `obs` text,
  `pass` text,
  `user` text,
  `url` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `service_name` varchar(100) NOT NULL DEFAULT 'project',
  `id_customer` int(11) NOT NULL DEFAULT '1',
  `id_user` int(11) NOT NULL DEFAULT '1',
  `type` varchar(100) NOT NULL,
  `stage` int(11) NOT NULL DEFAULT '0',
  `pipeline` int(1) NOT NULL DEFAULT '0',
  `deadline` int(100) NOT NULL,
  `create_in` int(100) NOT NULL,
  `startdate` int(100) DEFAULT NULL,
  `obs` varchar(1000) DEFAULT NULL,
  `hours` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projects`
--

INSERT INTO `projects` (`id`, `name`, `service_name`, `id_customer`, `id_user`, `type`, `stage`, `pipeline`, `deadline`, `create_in`, `startdate`, `obs`, `hours`) VALUES
(1, 'SISSA [SITE]', 'project', 1, 1, '0', 2, 1, 1472180400, 1472086276, 1470625200, 'http://sissa.maples.com.br/', 0),
(2, 'SM Services [SITE]', 'project', 1, 1, '0', 3, 0, 1472180400, 1472086375, 1470625200, 'www.smservices.com.br/dev', 0),
(3, 'Difordek [SITE]', 'project', 1, 1, '0', 1, 1, 1473994800, 1472086457, 1464750000, 'http://dev.difordek.com.br', 0),
(4, 'Auticomp [PLUGIN]', 'maintenance', 1, 1, '0', 3, 1, 1472180400, 1472086525, 1471230000, 'Plugin de inbound marketing para o site', 30),
(5, 'Autishop [LOJA]', 'project', 1, 1, '0', 2, 1, 1472612400, 1472086581, 1470020400, 'Faltam criar banners para o site (Nilton solicitou por e-mail ao Fabiano), e também criar o método de pagamento para vendas agenciadas.', 0),
(6, 'Marçalo', 'project', 15, 1, '0', 3, 0, 1467946800, 1472086746, 1467342000, '', 0),
(7, 'Bebê Magazine [ID VISUAL]', 'project', 1, 1, '2', 3, 0, 1472785200, 1472087237, 1471230000, '', 0),
(8, 'Tabloide Marçalo', 'project', 1, 2, '2', 0, 0, 1472439600, 1472129069, 1471402800, '', 0),
(9, 'Autishop [BANNERS]', 'project', 1, 1, '2', 3, 0, 1472094000, 1472150262, 1472094000, '', 0),
(10, 'Folha sys', 'project', 1, 1, '0', 0, 1, 1479434400, 1479162299, 1479088800, '', 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` tinyint(4) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_in` int(100) NOT NULL,
  `projects` longtext,
  `role` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `password`, `name`, `email`, `status`, `create_in`, `projects`, `role`) VALUES
(1, '455349befa0883e6810f31f8d840571f', 'Gil Vieira', 'gil@maples.com.br', 1, 1460065958, NULL, 0),
(2, '85206ee788333f621e1ea52de0c3badc', 'Fabiano Freitas', 'fabiano@maples.com.br', 1, 1460065958, NULL, 0),
(6, '85206ee788333f621e1ea52de0c3badc', 'Andrei Asse', 'andrei@maples.com.br', 1, 1460065958, NULL, 0),
(7, '85206ee788333f621e1ea52de0c3badc', 'Victor', 'victor@maples.com.br', 1, 1479168688, NULL, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allocation`
--
ALTER TABLE `allocation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkin`
--
ALTER TABLE `checkin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passwords`
--
ALTER TABLE `passwords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allocation`
--
ALTER TABLE `allocation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `checkin`
--
ALTER TABLE `checkin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
