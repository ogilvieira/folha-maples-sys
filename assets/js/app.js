$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('.selectpicker').selectpicker();
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        language: 'pt-BR',
        weekStart: 0,
        // startDate:'0d',
        todayHighlight: true
    });
    $('.input-percent').on('keyup', function(e){
        if(Number($(this).val().split('%')[0]) > 100){
            $(this).val('100%');
        }
    })

    if($('.form-project').length){
        $('.form-project select[name=stage]').on('change', function(){
            $('.form-project input[name=status]').val($('.form-project select[name=stage] option:checked').attr("data-percent")+"%");
        });
    };

    if($('form.form-customer').length){
        $('form.form-customer [name=monthly]').on('change', function(){
            if($('form.form-customer [name=monthly]').val() == '1'){
                $('form.form-customer .unlock-monthly').removeAttr('disabled');
            } else {
                $('form.form-customer .unlock-monthly').attr('disabled', 'disabled');
            }
        });
    };

    var setLoginSize = function(){
        if($(window).outerHeight() > 500){
            $('.login-wrap').height($(window).outerHeight());
        } else {
            $('.login-wrap').height('auto');
        }
    };

    if($('.login-wrap').length){
        setLoginSize();
        $(window).resize(function(){
            setLoginSize();
        });
    }
});


//helpers
$.fn.serializeFormJSON = function () {

    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
