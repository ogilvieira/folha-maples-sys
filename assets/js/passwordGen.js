var APP = APP || {};

APP.passwordgen = (function($){

    var obj = {};


    obj.init = function(){
      obj.container = $('form#passwordgen');
      obj.passresult = $('#passresult');
      obj.copybtn = new Clipboard('.btn-copy-result');
      obj.addEvents();
    };

    obj.submitData = function(data){

      $.ajax({
        url: SITEURL+'password/get',
        type: 'POST',
        dataType: 'json',
        data: data,
      })
      .done(function(res) {
        if(res.err == 0){
          obj.passresult.val(res.msg);
        }
      })
      .fail(function(res) {
        console.log(res);
      });
    };

    obj.addEvents = function(){
      obj.container.on('submit', function(e){
        e.preventDefault();
        obj.submitData(obj.container.serializeFormJSON());
      });

      obj.copybtn.on('success', function(e){
        alert("Copiado!");
        e.clearSelection();
      })
    };

    return obj;

})(jQuery || $);

$(document).ready(function(){
  APP.passwordgen.init();
});