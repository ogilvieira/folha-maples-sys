###################
FolhaSys
###################

Sistema de gerenciamento de projetos da Maples AGC. Feito em PHP com framework back-end Codeigniter, Mysql e framework front-end Bootstrap.

# Problemática:
Há hoje a necessidade de ter uma visão geral dos projetos para auxiliar na tomada de decisões e na organização de serviços.

# Com o sistema será possível:
  - Administrar projetos em 4 fases (pré-produção, produção, pós-produção e finalização)
  - Registrar empresas clientes e seus contatos.
  - Gerenciar colaboradores
  - Gerenciar serviços de manutenção
  - Gerar senhas seguras para credencias de projetos onde há envolvimento da Maples, baseado no username/email usado para acesso no painel de login

