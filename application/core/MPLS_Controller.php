<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MPLS_Controller extends CI_Controller {
	public $template = 'layout/default';
	public $css = array();
	public $js = array();
	public $title;
	public $metas = array();
    public function verifyCredentials(){
    	if($this->session->userdata('logged_in')):
    		return $this->session->userdata('logged_in');
	    else:
	    	return false;
    	endif;
    }

	public function __construct() {
		parent::__construct();

		//set default date params
		$this->load->helper('date');
    date_default_timezone_set('America/Sao_Paulo');

    $this->load->model('UserModel','',TRUE);

    $this->__css = $this->config->item('default_css');
    $this->__js = $this->config->item('default_js');
        $this->title       = "FolhaSys";
    $this->session_data = false;
    $this->body_classes = "page-default";
    $this->menu_target = "dashboard";

    if($this->router->class):
      $this->body_classes = "page-".$this->router->class;
    endif;
    if($this->router->method):
      $this->body_classes = $this->body_classes." page-method-".$this->router->method;
    endif;

    $this->load->library('breadcrumbs');
    $this->breadcrumbs->push('<i class="glyphicon glyphicon-home"></i>', '/');

        if(!property_exists($this, "checkUser")):
			$this->checkUser = true;
		endif;

        if($this->checkUser):
        	$this->session_data = $this->verifyCredentials();

        	if(!$this->session_data):
        		redirect('login', 'refresh');
        	elseif(!$this->UserModel->getSatus($this->session_data['id'])):
        		redirect('logout', 'refresh');
        	endif;
        endif;
	}

	public function view($page, $data=array()){
		$data['page_content'] = $this->load->view($page, $data, true);
		$this->load->view($this->template, $data);
	}
}

/* End of file MPLS_Controller.php */
/* Location: ./application/core/MPLS_Controller.php */