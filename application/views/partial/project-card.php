<section>
  <div class="row">
    <?php if($projects): ?>
    <?php foreach($projects as $key => $project): ?>
    <?php switch ($project->stage) {
      case '0':
        $project->status_class = "info";
        break;
      case '1':
        $project->status_class = "success";
        break;
      case '2':
        $project->status_class = "primary";
        break;
      case '3':
        $project->status_class = "default";
        break;
      default:
        $project->status_class = "default";
        break;
    }
    ?>
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-<?php echo $project->status_class; ?>">
          <div class="panel-heading">
            <h3 class="panel-title">
              <a href="<?php echo base_url('/project/detail/'.$project->id); ?>"><?php echo $project->name; ?></a><br>
              <?php switch ($project->stage) {
                case '0':
                  echo "<small>Pré produção</small>";
                  break;
                case '1':
                  echo "<small>Produção</small>";
                  break;
                case '2':
                  echo "<small>Pós produção</small>";
                  break;
                default:
                  echo "<small>Finalizado</small>";
                  break;
              }
              ?>
              <br>
            </h3>
          </div>
          <div class="panel-body">
            <p><strong><i class="glyphicon glyphicon-time"></i> Tempo:</strong> 230/<?php echo $project->hours;?>hs</p>
            <div class="progress">
              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%">
                <span class="sr-only">30% Complete</span>
              </div>
            </div>
            <p><strong><i class="glyphicon glyphicon-calendar"></i> Deadline:</strong> <?php echo date("d/m/Y", $project->deadline); ?></p>
            <p><strong><i class="glyphicon glyphicon-user"></i> Responsável:</strong> <?php echo $this->UserModel->get($project->id_user)['name']; ?></p>
          </div>
          <div class="panel-footer">
            <a href="<?php echo base_url('/project/detail/'.$project->id); ?>" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-eye-open"></i> Detalhes</a>
            <a href="<?php echo base_url('/project/edit/'.$project->id); ?>" class="btn">Editar</a>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
    <?php else: ?>
      <div class="col-lg-12">
          <div class="alert alert-warning text-center">
            <p>
              Não foram encontrados <strong>projetos</strong>.<br>
            </p>
          </div>
      </div>
    <?php endif; ?>

  </div>
</section>