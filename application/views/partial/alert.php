<div class="row">
    <div class="col-lg-12">
    <?php if($this->session->flashdata('success_message')): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('danger_message')): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('danger_message'); ?>
        </div>
    <?php endif; ?>
    </div>
</div>
