<?php if($pagination): ?>
	<?php 
	if($pagination['page_total'] > 1): ?>
	<div class="row">
		<div class="col col-lg-12">
			<nav class="text-center">
			  <ul class="pagination">
			  	<?php if($pagination['page_active'] > 1): ?>
			    <li>
			      <a href="<?php echo "?p=".($pagination['page_active']-1)."&l=".$pagination['page_limit'].""; ?>" aria-label="Previous">
			        <span aria-hidden="true">&laquo;</span>
			      </a>
			    </li>
				<?php endif; ?>
			    <?php for($i=1; $i <= ($pagination['page_total']-1 > 5 ? 5 : $pagination['page_total']); $i++): ?>
			    <li <?php echo ($i == $pagination['page_active']) ? 'class="active"' : ''; ?>><a href="<?php echo "?p=".$i."&l=".$pagination['page_limit']; ?>"><?php echo $i; ?></a></li>
				<?php endfor; ?>
				<?php if($pagination['page_active'] < $pagination['page_total']): ?>
			    <li>
			      <a href="<?php echo "?p=".($pagination['page_active']+1)."&l=".$pagination['page_limit'].""; ?>" aria-label="Next">
			        <span aria-hidden="true">&raquo;</span>
			      </a>
			    </li>
				<?php endif; ?>
			  </ul>
			</nav>
		</div>
	</div>
	<?php endif; ?>
<?php endif; ?>