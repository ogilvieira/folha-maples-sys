<div class="container-fluid">
	<div class="col-xs-6 col-md-6 col-lg-3">
		<div class="panel panel-teal panel-widget">
			<div class="row no-padding">
				<div class="col-sm-3 col-lg-5 widget-left">
					<svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg>
				</div>
				<div class="col-sm-9 col-lg-7 widget-right">
					<div class="large">120</div>
					<div class="text-muted">Pré produção</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-6 col-md-6 col-lg-3">
		<div class="panel panel-red panel-widget">
			<div class="row no-padding">
				<div class="col-sm-3 col-lg-5 widget-left">
					<svg class="glyph stroked app-window"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-app-window"></use></svg>
				</div>
				<div class="col-sm-9 col-lg-7 widget-right">
					<div class="large">52</div>
					<div class="text-muted">Produção</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-6 col-md-6 col-lg-3">
		<div class="panel panel-orange panel-widget">
			<div class="row no-padding">
				<div class="col-sm-3 col-lg-5 widget-left">
					<svg class="glyph stroked app-window-with-content"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-app-window-with-content"></use></svg>
				</div>
				<div class="col-sm-9 col-lg-7 widget-right">
					<div class="large">24</div>
					<div class="text-muted">Pós produção</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-6 col-md-6 col-lg-3">
		<div class="panel panel-blue panel-widget">
			<div class="row no-padding">
				<div class="col-sm-3 col-lg-5 widget-left">
					<svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"/></svg>
				</div>
				<div class="col-sm-9 col-lg-7 widget-right">
					<div class="large">25.2k</div>
					<div class="text-muted">Finalizados</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /projects-satatus-bar -->