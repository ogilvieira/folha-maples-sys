<?php if($taskboard): ?>
<div class="table-responsive">
<table class="table table-bordered">
<tr>
  <td>Nome</td>
  <td>Cliente</td>
  <td>Status</td>
  <td>Tipo de Serviço</td>
  <td>Horas</td>
  <td>Deadline</td>
  <td>Ações</td>
</tr>
<?php foreach($taskboard as $key => $task): ?>
<?php switch ($task->stage) {
  case '0':
    $task->status_class = "info";
    break;
  case '1':
    $task->status_class = "success";
    break;
  case '2':
    $task->status_class = "primary";
    break;
  case '3':
    $task->status_class = "default";
    break;
  default:
    $task->status_class = "default";
    break;
}
?>
<tr class="<?php echo "bg-" . $task->status_class; ?>">
  <td>
    <a href="<?php echo base_url('/'.$task->service_name.'/detail/'.$task->id); ?>" target="_blank"><?php echo $task->name; ?></a>
  </td>
  <td>
    <a href="<?php echo base_url('/customer/detail/'.$task->id_customer); ?>" target="_blank"><?php echo $this->CustomerModel->get($task->id_customer)['name']; ?> <i class="glyphicon glyphicon-new-window"></i></a>
  </td>
  <td>
    <?php switch ($task->stage) {
      case '0':
        echo "Pré produção";
        break;
      case '1':
        echo "Produção";
        break;
      case '2':
        echo "Pós produção";
        break;
      default:
        echo "Finalizado";
        break;
    }
    ?>
  </td>
  <td><?php echo ($task->service_name == 'project') ? 'Projeto' : 'Manutenção'; ?></td>
  <td>10/<?php echo $task->hours;?>hs</td>
  <td><?php echo date("d/m/Y", $task->deadline); ?></td>
  <td>
    <?php if(!$task->allocation_id): ?>
    <a href="#" class="btn btn-default">Checkin</a>
    <?php else: ?>
    <a href="#" class="btn btn-default">Checkout</a>
    <?php endif; ?>
  </td>
</tr>
<?php endforeach; ?>
</table>
</div>
<?php else: ?>
  <div class="col-lg-12">
      <div class="alert alert-warning text-center">
        <p>
          Não foram encontrados <strong>projetos</strong>.<br>
        </p>
      </div>
  </div>
<?php endif; ?>