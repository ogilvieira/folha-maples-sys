<div class="container-fluid">
	<div class="col-lg-12">
		<div class="form-group">
			<a href="<?php echo base_url('user/add'); ?>" class="btn btn-success">Novo usuário</a>
		</div>
	</div>
</div>
<?php if($users): ?>
<div class="container-fluid">
	<div class="col-lg-12">

		<div class="panel panel-default">
		  <!-- Default panel contents -->
		  <div class="panel-heading">Lista de Usuários</div>

		  <!-- Table -->
		  <table class="table">
		    <thead>
		    	<tr>
			    	<th>Nome</th>
			    	<th>E-mail</th>
			    	<th>Status</th>
			    	<th>Criado em</th>
			    	<th></th>
		    	</tr>
			</thead>
			<tbody>
				<?php foreach ($users as $key => $user): ?>
				<tr>
					<td><?php echo $user->name; ?></td>
					<td><?php echo $user->email; ?></td>
					<td>
						<?php if($user->status): ?>
						<span class="label label-success">Ativo</span>
						<?php else: ?>
						<span class="label label-danger">Desativado</span>
						<?php endif; ?>
					</td>
					<td><?php echo mdate("%d/%m/%Y", $user->create_in); ?></td>
					<td><a href="<?php echo base_url('user/edit/'.$user->id); ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		  </table>
		</div>
		<?php 
			$this->load->view('partial/pagination');
		?>
	</div>
</div><!--/.row-->
<?php else: ?>
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-info">
		  <strong>Ops!</strong> Nenhum usuário encontrado.
		</div>
	</div>
</div>
<?php endif; ?>