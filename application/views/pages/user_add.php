<div class="container-fluid">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Novo usuário</div>
			<div class="panel-body">
				<form action="<?php echo base_url('user/add'); ?>" method="post">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="">Nome:</label>
								<input type="text" class="form-control" name="name" placeholder="Nome" minlength="5" value="<?php echo (isset($userData['name'])) ? $userData['name'] : ''; ?>" required>
							</div>
							<div class="form-group">
								<label for="">E-mail:</label>
								<input type="email" class="form-control" name="email" placeholder="E-mail" value="<?php echo (isset($userData['email'])) ? $userData['email'] : ''; ?>" required>
							</div>
							<div class="form-group">
								<label for="">Status:</label>
								<select class="form-control" name="status">
									<option <?php if(isset($userData['status'])){ echo ($userData['status'] == 1) ? "selected" : ''; } ?> value="1">Usuário Ativo</option>
									<option <?php if(isset($userData['status'])){ echo ($userData['status'] == 0) ? "selected" : ''; } ?> value="0">Usuário Desativado</option>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="">Senha:</label>
								<input type="password" class="form-control" name="password" minlength="6" placeholder="Senha">
							</div>
							<div class="form-group">
								<label for="">Repetir Senha:</label>
								<input type="password" class="form-control" name="password_compare" minlength="6" placeholder="Repetir senha">
							</div>
							<div class="form-group">
								<label for="">Nível de Acesso:</label>
								<select class="form-control" name="role">
									<option value="0">Staff</option>
									<option value="1">Gerente</option>
									<option value="2">Financeiro</option>
									<option value="3">Administrador</option>
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<a href="<?php echo base_url('user'); ?>" class="btn btn-default">Cancelar</a>
								<button type="reset" class="btn btn-default">Resetar</button>
								<button type="submit" class="btn btn-success btn-right">Salvar</button>
							</div>
						</div>
					</div>
				</form>
				<div class="row">
					<div class="col-lg-12">
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!--/.row-->
