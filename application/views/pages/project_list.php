<div class="container-fluid">
	<div class="col-sm-3">
			<form class="form" method="get" action="<?php echo base_url('project'); ?>">
				
				<div class="input-group">
					<input type="text" name="n" class="form-control" placeholder="Nome do projeto" value="<?php echo $query['name']; ?>">
		      <span class="input-group-btn">
						<button class="btn btn-primary" type="submit">Ir!</button>
		      </span>
				</div>
				<hr>
				<div class="form-group">
					<select class="form-control" name="s">
						<option value="">Estágio</option>
						<option value="0" <?php if($query['stage'] == '0') echo "selected"; ?>>Pré-produção</option>
						<option value="1" <?php if($query['stage'] == '1') echo "selected"; ?>>Produção</option>
						<option value="2" <?php if($query['stage'] == '2') echo "selected"; ?>>Pós-Produção</option>
						<option value="3" <?php if($query['stage'] == '3') echo "selected"; ?>>Finalizado</option>
					</select>
				</div>
				<?php if($customers): ?>
				<div class="form-group">
					<select class="form-control selectpicker" data-live-search="true" name="c">
						<option value="">Cliente</option>
						<optgroup label="Selecione o cliente">
							<?php foreach ($customers as $key => $customer): ?>
								<option value="<?php echo $customer->id; ?>" <?php if($query['id_customer'] == $customer->id){ echo "selected"; } ?>><?php echo $customer->name; ?></option>
							<?php endforeach; ?>
						</optgroup>
					</select>
				</div>
				<?php endif; ?>
				<div class="form-group">
					<div class="checkbox">
						<label><input type="checkbox" name="hide3" <?php if($query['hide3']) echo "checked"; ?>> Esconder finalizados</label>
					</div>
				</div>
				<a href="<?php echo base_url('project'); ?>" class="btn btn-block btn-default"><i class="glyphicon glyphicon-trash"></i> Limpar Filtros</a>
			</form>
			<hr>
	</div>
	<div class="col-sm-9">
		<?php $this->load->view('partial/project-card'); ?>
		<?php $this->load->view('partial/pagination');?>
	</div>
</div>
