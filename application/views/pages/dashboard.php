<div class="container-fluid">
  <div class="col-lg-12">
<?php $this->load->view('partial/project-general-status'); ?>
  </div>
</div>
<div class="container-fluid">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h2 class="panel-title">Taskboard: (<?php echo count($taskboard); ?>)</h2>
      </div>
      <div class="panel-body">
        <?php $this->load->view('partial/taskboard-card'); ?>
      </div>
    </div>
  </div>
</div>
<!-- /.taskboard -->
<div class="container-fluid">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h2 class="panel-title">Últimas atividades:</h2>
      </div>
      <div class="panel-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td><strong>Usuário</strong></td>
              <td><strong>Ação</strong></td>
              <td><strong>Horário</strong></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Gil Vieira</td>
              <td>Alterou dados do projeto</td>
              <td>5 dias atrás</td>
            </tr>
            <tr>
              <td>Gil Vieira</td>
              <td>Alterou dados do projeto</td>
              <td>5 dias atrás</td>
            </tr>
            <tr>
              <td>Gil Vieira</td>
              <td>Alterou dados do projeto</td>
              <td>5 dias atrás</td>
            </tr>
            <tr>
              <td>Gil Vieira</td>
              <td>Alterou dados do projeto</td>
              <td>5 dias atrás</td>
            </tr>
            <tr>
              <td>Gil Vieira</td>
              <td>Alterou dados do projeto</td>
              <td>5 dias atrás</td>
            </tr>
            <tr>
              <td>Gil Vieira</td>
              <td>Alterou dados do projeto</td>
              <td>5 dias atrás</td>
            </tr>
            <tr>
              <td>Gil Vieira</td>
              <td>Alterou dados do projeto</td>
              <td>5 dias atrás</td>
            </tr>
            <tr>
              <td>Gil Vieira</td>
              <td>Alterou dados do projeto</td>
              <td>5 dias atrás</td>
            </tr>
            <tr>
              <td>Gil Vieira</td>
              <td>Alterou dados do projeto</td>
              <td>5 dias atrás</td>
            </tr>
            <tr>
              <td>Gil Vieira</td>
              <td>Alterou dados do projeto</td>
              <td>5 dias atrás</td>
            </tr>
            <tr>
              <td>Gil Vieira</td>
              <td>Alterou dados do projeto</td>
              <td>5 dias atrás</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>