<div class="container-fluid">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Editar Cliente</div>
			<div class="panel-body">
				<form class="form-customer" action="<?php echo base_url('customer/edit/'.$customerData['id']); ?>" method="post">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="">Nome do cliente: <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="name" placeholder="Nome" minlength="5" value="<?php echo (isset($customerData['name'])) ? $customerData['name'] : ''; ?>">
							</div>
							<div class="form-group">
								<label for="">Mensalista</label>
								<select name="monthly" class="form-control">
									<option value="0" <?php echo ($customerData['monthly'] == '0') ? 'selected' : ''; ?>>Não</option>
									<option value="1" <?php echo ($customerData['monthly'] == '1') ? 'selected' : ''; ?>>Sim</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">Dia de aniversário do plano:</label>
								<select name="birth" class="form-control unlock-monthly" <?php echo ($customerData['monthly'] == '0') ? 'disabled=disabled' : ''; ?> required>
									<option value="5" <?php echo ($customerData['birth'] == '5') ? 'selected' : ''; ?>>5</option>
									<option value="10" <?php echo ($customerData['birth'] == '10') ? 'selected' : ''; ?>>10</option>
									<option value="15" <?php echo ($customerData['birth'] == '15') ? 'selected' : ''; ?>>15</option>
									<option value="20" <?php echo ($customerData['birth'] == '20') ? 'selected' : ''; ?>>20</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">Horas/mês:</label>
								<input type="text" class="form-control unlock-monthly" name="hs" placeholder="Horas" value="<?php echo (isset($customerData['hs'])) ? $customerData['hs'] : ''; ?>" data-mask="0000" <?php echo ($customerData['monthly'] == '0') ? 'disabled=disabled' : ''; ?> required>
							</div>
							<div class="form-group">
								<label for="">Bloquear plano em:</label>
								<input type="text" name="date_plan" class="form-control unlock-monthly datepicker" placeholder="" value="<?php echo $customerData['date_plan'] ? date("d/m/Y", $customerData['date_plan']) : ''; ?>" <?php echo ($customerData['monthly'] == '0') ? 'disabled=disabled' : ''; ?> required>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<a href="<?php echo base_url('customer/detail/' . $customerData["id"]); ?>" class="btn btn-default"><i class="glyphicon glyphicon-ban-circle"></i> Cancelar edição</a>
								<button type="submit" class="btn btn-success btn-right"><i class="glyphicon glyphicon-floppy-disk"></i> Salvar</button>
							</div>
						</div>
					</div>
					
				</form>
				<div class="row">
					<div class="col-lg-12">
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!--/.row-->
