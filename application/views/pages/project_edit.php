<div class="container-fluid">
  <div class="col-lg-12">
        <form action="<?php echo base_url('project/edit/'.$projectData['id']); ?>" class="form-project" method="post">
<div class="row">
        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Básico</h3>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <label for="">Nome do Projeto:</label>
                <input type="text" class="form-control" name="name" placeholder="" minlength="3" value="<?php echo (isset($projectData['name'])) ? $projectData['name'] : ''; ?>" requiredclass="form-project" >
              </div>

              <div class="form-group">
                <label for="">Tipo:</label>
                <select class="form-control" name="type" id="">
                  <option value="0" <?php echo ($projectData['type'] == "0") ? 'selected':''; ?>>Web</option>
                  <option value="2" <?php echo ($projectData['type'] == "2") ? 'selected':''; ?>>Design</option>
                  <option value="1" <?php echo ($projectData['type'] == "1") ? 'selected':''; ?>>Marketing</option>
                  <option value="3" <?php echo ($projectData['type'] == "3") ? 'selected':''; ?>>Outro</option>
                </select>
              </div>

              <div class="form-group">
                <label for="">Cliente:</label>
                <select class="form-control selectpicker" name="id_customer" data-live-search="true">
                  <?php foreach ($customers as $key => $customer): ?>
                    <option value="<?php echo $customer->id; ?>" <?php if(1 == $customer->id){ echo "selected"; } ?>><?php echo $customer->name; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>

              <div class="form-group">
                <label for="">Observações:</label>
                <textarea name="obs" class="form-control" cols="30" rows="4"><?php echo ($projectData['obs']) ? $projectData['obs']:''; ?></textarea>
              </div>

            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Produção</h3>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <select class="form-control" name="stage" id="">
                  <option value="0" <?php echo ($projectData['stage'] == "0") ? 'selected':''; ?> data-percent="0">Pré-produção</option>
                  <option value="1" <?php echo ($projectData['stage'] == "1") ? 'selected':''; ?> data-percent="10">Produção</option>
                  <option value="2" <?php echo ($projectData['stage'] == "2") ? 'selected':''; ?> data-percent="75">Pós-Produção</option>
                  <option value="3" <?php echo ($projectData['stage'] == "3") ? 'selected':''; ?> data-percent="100">Finalizado</option>
                </select>
              </div>

              <div class="form-group">
                <label for="">Data de entrada:</label>
                <input type="text" name="startdate" class="form-control datepicker" value="<?php echo date("d/m/Y", $projectData['startdate']); ?>" placeholder="00/00/0000" required>
              </div>
              <div class="form-group">
                <label for="">Deadline (data para entrega):</label>
                <input type="text" name="deadline" class="form-control datepicker" value="<?php echo date("d/m/Y", $projectData['deadline']); ?>" placeholder="00/00/0000" required>
              </div>
              <div class="form-group">
                <label for="">Horas contratadas:</label>
                <input type="text" class="form-control" name="hours" value="<?php echo $projectData['hours']; ?>" data-mask="000hs" maxlength="5" data-mask-reverse="true">
              </div>
            </div>
          </div>

        </div>
        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Delegar</h3>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <label for="">Profissional responsável:</label>
                <?php if($projectData['allocation_id']): ?>
                  <?php foreach ($users as $key => $user): ?>
                    <?php if($projectData['id_user'] == $user->id){ echo "<div class='alert alert-danger'>Só pode ser alterado quando <strong>". $user->name . "</strong> marcar <u>checkout</u> na projeto.</div>"; } ?>
                  <?php endforeach; ?>
                <?php else: ?>
                <select class="form-control selectpicker" name="id_user" data-live-search="true">
                  <?php foreach ($users as $key => $user): ?>
                    <option value="<?php echo $user->id; ?>" <?php if(0 == $user->id){ echo "selected"; } ?>><?php echo $user->name; ?></option>
                  <?php endforeach; ?>
                </select>
                <?php endif; ?>
              </div>
              
              <div class="form-group">
                <label for="">Exibir no pipeline:</label>
                <?php if($projectData['allocation_id']): ?>
                  <div class='alert alert-danger'>Indisponível.</div>
                <?php else: ?>
                <select class="form-control" name="pipeline">
                  <option value="0" <?php echo ($projectData['pipeline'] == "0") ? 'selected':''; ?>>Não</option>
                  <option value="1" <?php echo ($projectData['pipeline'] == "1") ? 'selected':''; ?>>Sim</option>
                </select>
                <?php endif; ?>
              </div>

            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="form-group">
            <?php if(!$projectData['allocation_id']): ?>
            <a href="<?php echo base_url('project/detail/'.$projectData['id']); ?>" class="btn btn-lg btn-default"><i class="glyphicon glyphicon-trash"></i> Excluir</a>
            <?php endif; ?>
            <a href="<?php echo base_url('project/detail/'.$projectData['id']); ?>" class="btn btn-lg btn-default"><i class="glyphicon glyphicon-ban-circle"></i> Cancelar</a>
            <button type="submit" class="btn btn-lg btn-success btn-right"><i class="glyphicon glyphicon-floppy-disk"></i> Salvar</button>
          </div>
        </div>
      </div>
        </form>
        <div class="row">
          <div class="col-lg-12">
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!--/.row-->