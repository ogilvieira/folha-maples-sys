<div class="container-fluid">
	<div class="col-lg-3">
		<form class="form" method="get" action="<?php echo base_url('customer'); ?>">
			<div class="input-group">
				<input type="text" name="n" class="form-control" placeholder="Nome do cliente" value="<?php echo $query['name']; ?>">
	      <span class="input-group-btn">
					<button class="btn btn-primary" type="submit">Ir!</button>
	      </span>
			</div>
			<hr>
			<div class="form-group">
				<div class="checkbox">
					<label><input type="checkbox" name="m" value="1" <?php if($query['monthly']) echo "checked"; ?>> Mensalistas</label>
				</div>
			</div>
			<a href="<?php echo base_url('project'); ?>" class="btn btn-block btn-default"><i class="glyphicon glyphicon-trash"></i> Limpar Filtros</a>
		</form>
	</div>
<?php if($customers): ?>
	<div class="col-lg-9">

		  <table class="table table-hover">
		    <thead>
		    	<tr>
			    	<th>Empresa</th>
			    	<th>Plano Mensal</span></th>
			    	<th class="text-center">Hs gastas</span></th>
			    	<th class="text-center">Próx. vencimento</span></th>
			    	<th></th>
		    	</tr>
			</thead>
			<tbody>
				<?php foreach ($customers as $key => $customer): ?>
				<tr>
					<td><strong><a href="<?php echo base_url('customer/detail/'.$customer->id); ?>"><?php echo $customer->name; ?></a></strong></td>
					<!-- <td><?php //echo mdate("%d/%m/%Y", $customer->create_in); ?></td> -->
					<td>

						<?php if($customer->monthly): ?>
							<?php if(strtotime("0:00",time()) > strtotime("0:00", $customer->date_plan)): ?>
								<?php echo ($customer->hs && $customer->date_plan) ? '<span class="text-danger">Cancelado em ' . date("d/m/Y", $customer->date_plan) . '</span>' : 'Não Contratado'; ?></td>
							<?php else: ?>
								<?php echo ($customer->hs) ? '<strong>' . $customer->hs . ' horas</strong>' : 'Não contratado'; ?></td>
							<?php endif; ?>
						<?php else: ?>
						Não Contratado
						<?php endif; ?>

					<td class="text-center">----</td>
					<td class="text-center">
						<?php if($customer->monthly): ?>
							<?php if(strtotime("0:00",time()) > strtotime("0:00", $customer->date_plan)): ?>
								<?php echo $customer->birth.'/'. (($customer->birth > date("d")) ? date("m") : date("m", strtotime('+1 months'))); ?>
							<?php else: ?>
								----
							<?php endif; ?>
						<?php else: ?>
							----
						<?php endif; ?>
					</td>
					<td>
						<a href="<?php echo base_url('customer/edit/'.$customer->id); ?>" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-pencil"></span> EDITAR</a>
						<a href="<?php echo base_url('customer/detail/'.$customer->id); ?>" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> VER</a>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		  </table>
		<?php $this->load->view('partial/pagination'); ?>
<?php else: ?>
	<div class="col-lg-9">
		<div class="alert alert-info">
		  <strong>Ops!</strong> Nenhum cliente encontrado.
		</div>
	</div>
<?php endif; ?>
</div>