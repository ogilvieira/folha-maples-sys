<div class="container-fluid">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Novo cliente</div>
			<div class="panel-body">
				<form class="form-customer" action="<?php echo base_url('customer/add'); ?>" method="post">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<input type="text" class="form-control" name="name" placeholder="Nome" minlength="5" value="<?php echo (isset($customerData['name'])) ? $customerData['name'] : ''; ?>">
							</div>
							
							<div class="form-group">
								<label for="">Mensalista</label>
								<select name="monthly" class="form-control">
									<option value="0">Não</option>
									<option value="1">Sim</option>
								</select>
							</div>
							
							<div class="form-group">
								<label for="">Dia de aniversário do plano:</label>
								<select name="birth" class="form-control unlock-monthly" disabled="disabled" required>
									<option value="5">5</option>
									<option value="10">10</option>
									<option value="15">15</option>
									<option value="20">20</option>
								</select>
							</div>

							<div class="form-group">
								<label for="">Horas/mês:</label>
								<input type="text" class="form-control unlock-monthly" name="hs" placeholder="Horas" value="" data-mask="0000" disabled="disabled" required>
							</div>

							<div class="form-group">
								<label for="">Bloquear plano em:</label>
								<input type="text" name="date_plan" class="form-control unlock-monthly datepicker" placeholder="" value="" disabled="disabled" required>
							</div>

						</div>
						<div class="col-lg-12">							
							<div class="form-group">
								<a href="<?php echo base_url('customer'); ?>" class="btn btn-danger">Cancelar</a>
								<button type="submit" class="btn btn-success btn-right">Salvar</button>
							</div>
						</div>
					</div>
					
				</form>
				<div class="row">
					<div class="col-lg-12">
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!--/.row-->
