<div class="container-fluid">
	<div class="col-lg-12">
		<form action="<?php echo base_url('project/add'); ?>" class="form-project" method="post">
			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Básico</h3>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label for="">Nome do Projeto:</label>
								<input type="text" class="form-control" name="name" placeholder="" minlength="3" value="<?php echo (isset($projectData['name'])) ? $projectData['name'] : ''; ?>" requiredclass="form-project" >
							</div>

							<div class="form-group">
								<label for="">Tipo:</label>
								<select class="form-control" name="type" id="">
									<option value="2">Design</option>
									<option value="0">Web</option>
									<option value="1">Marketing</option>
									<option value="3">Outro</option>
								</select>
							</div>

							<div class="form-group">
								<label for="">Cliente:</label>
								<select class="form-control selectpicker" name="id_customer" data-live-search="true">
									<?php foreach ($customers as $key => $customer): ?>
										<option value="<?php echo $customer->id; ?>" <?php if(1 == $customer->id){ echo "selected"; } ?>><?php echo $customer->name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>

							<div class="form-group">
								<label for="">Observações:</label>
								<textarea name="obs" class="form-control" cols="30" rows="4"></textarea>
							</div>

						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Produção</h3>
						</div>
						<div class="panel-body">
							<input type="hidden" name="stage" value="0">
							<div class="form-group">
								<label for="">Data de entrada:</label>
								<input type="text" name="startdate" class="form-control datepicker" placeholder="00/00/0000" required>
							</div>
							<div class="form-group">
								<label for="">Deadline (data para entrega):</label>
								<input type="text" name="deadline" class="form-control datepicker" placeholder="00/00/0000" required>
							</div>
							<div class="form-group">
								<label for="">Horas contratadas:</label>
								<input type="text" class="form-control" name="hours" value="0" data-mask="000hs" maxlength="5" data-mask-reverse="true">
							</div>
						</div>
					</div>

				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Delegar</h3>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label for="">Profissional responsável:</label>
								<select class="form-control selectpicker" name="id_user" data-live-search="true">
									<?php foreach ($users as $key => $user): ?>
										<option value="<?php echo $user->id; ?>" <?php if(0 == $user->id){ echo "selected"; } ?>><?php echo $user->name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							
							<div class="form-group">
								<label for="">Exibir no pipeline:</label>
								<select class="form-control" name="pipeline">
									<option value="0">Não</option>
									<option value="1">Sim</option>
								</select>
							</div>

						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<a href="<?php echo base_url('project'); ?>" class="btn btn-lg btn-danger">Cancelar</a>
						<button type="submit" class="btn btn-lg btn-success btn-right">Salvar</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div><!--/.row-->
