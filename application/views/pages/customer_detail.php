<div class="container-fluid">
	<div class="col-lg-12">
		<div class="row">

		<?php if($customer['monthly']):; ?>
		<?php if(strtotime("0:00",time()) > strtotime("0:00", $customer['date_plan'])): ?>
			<div class="col-sm-12">
				<div class="alert alert-danger">
					O plano deste cliente foi <strong>Cancelado ou Bloqueado</strong> por data. <a href="<?php echo base_url('/customer/edit/'.$customer['id']); ?>"><u>Clique Aqui</u></a> para alterar a data do contrato.
				</div>
			</div>
		<?php endif; ?>

			<div class="col-lg-3">
				<div class="panel panel-default">
					<!-- Default panel contents -->
					<div class="panel-heading">
						<a href="<?php echo base_url('/customer/edit/'.$customer['id']); ?>" class="btn btn-success btn-xs pull-right"><span class="glyphicon glyphicon-pencil"></span> Editar</a>
						<i class="glyphicon glyphicon-time"></i> Plano:
					</div>
					<div class="panel-body">
							<?php echo ($customer['hs']) ? '<strong>' . $customer['hs'] . ' horas</strong>' : 'Não contratado'; ?></td>
					</div>
				</div>
			</div>

			<div class="col-lg-3">
				<div class="panel panel-default">
					<!-- Default panel contents -->
					<div class="panel-heading">
						<a href="<?php echo base_url('/customer/edit/'.$customer['id']); ?>" class="btn btn-success btn-xs pull-right"><span class="glyphicon glyphicon-pencil"></span> Editar</a>
						<i class="glyphicon glyphicon-calendar"></i> Fim do Plano:
					</div>
					<div class="panel-body">
						<?php echo date("d/m/Y", $customer['date_plan']); ?>
						<?php echo ($customer['hs'] && $customer['date_plan']) ? '<span class="text-danger">(Cancelado/Finalizado)</span>' : 'Não Contratado'; ?></td>
					</div>
				</div>
			</div>

			<div class="col-lg-3">
				<div class="panel panel-default">
					<!-- Default panel contents -->
					<div class="panel-heading">
						<a href="<?php echo base_url('/customer/edit/'.$customer['id']); ?>" class="btn btn-success btn-xs pull-right"><span class="glyphicon glyphicon-pencil"></span> Editar</a>
						<i class="glyphicon glyphicon-calendar"></i>
						<?php if(strtotime("0:00",time()) > strtotime("0:00", $customer['date_plan'])): ?>
							Último vencimento:
						<?php else: ?>
							Próx. vencimento:
						<?php endif; ?>
					</div>
					<div class="panel-body">
						<?php if(strtotime("0:00",time()) > strtotime("0:00", $customer['date_plan'])): ?>
							<?php echo $customer['birth'].'/'. date("m/y", $customer['date_plan']); ?>
						<?php else: ?>
							<?php echo $customer['birth'].'/'. (($customer['birth'] > date("d")) ? date("m/y") : date("m/y", strtotime('+1 months'))); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>

			<div class="col-lg-3">
				<div class="panel panel-default">
					<!-- Default panel contents -->
					<div class="panel-heading">
						<i class="glyphicon glyphicon-time"></i> Tempo gasto:
					</div>
					<div class="panel-body">
						<strong>120hs</strong>
					</div>
				</div>
			</div>
			<?php else: ?>
			<div class="col-sm-12">
				<div class="alert alert-info">
					Este cliente <strong>não é mensalista :(</strong>
				</div>
			</div>
			<?php endif; ?>


			<div class="col-lg-12">
				<section>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#projects" aria-controls="projects" role="tab" data-toggle="tab">Projetos</a></li>
				    <li role="presentation"><a href="#maintenance" aria-controls="maintenance" role="tab" data-toggle="tab">Manutenções</a></li>
				    <li role="presentation"><a href="#passwords" aria-controls="passwords" role="tab" data-toggle="tab">Senhas Salvas</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane fade in active" id="projects">
				    	<br>
								<?php $this->load->view('partial/project-card'); ?>
								<?php if($projects): ?>
								<hr>
								<a href="<?php echo base_url('project?c='.$customer['id']); ?>" target="_blank" class="btn btn-lg btn-primary"> Ver todos</a>
								<?php endif; ?>
				    </div>
				    <div role="tabpanel" class="tab-pane fade" id="maintenance">
				    	<br>
				    	em breve...
							<hr>
							<a href="#" class="btn btn-lg btn-primary" disabled> Ver todos</a>
				    </div>
				    <div role="tabpanel" class="tab-pane fade" id="passwords">
				    	<br>
				    	<div class="table-responsive">
							  <!-- Table -->
							  <table class="table table-bordered table-striped">
							    <thead>
							    	<tr>
								    	<th>Titulo</th>
								    	<th>Última alteração</th>
								    	<th>Tipo</th>
								    	<th></th>
							    	</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												Lorem ipsum dolor.
											</td>
											<td>
												10/10/2022
											</td>
											<td>
												Lorem ipsum dolor.
											</td>
											<td class="text-right">
												<a href="#" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-pencil"></span> EDITAR</a>
												<a href="#" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> VER</a>
											</td>
										</tr>
										<tr>
											<td>
												Lorem ipsum dolor.
											</td>
											<td>
												10/10/2022
											</td>
											<td>
												Lorem ipsum dolor.
											</td>
											<td class="text-right">
												<a href="#" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-pencil"></span> EDITAR</a>
												<a href="#" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> VER</a>
											</td>
										</tr>
									</tbody>
								</table>
					    </div>
							<hr>
							<a href="#" class="btn btn-lg btn-primary"> Add nova</a>
				    </div>
				  </div>

				</section>
				<hr>
			</div>


			<div class="col-lg-12">
				<div class="panel panel-default">
					<!-- Default panel contents -->
					<div class="panel-heading">
						<i class="glyphicon glyphicon-list"></i> Log:</strong>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr>
										<td><strong>Usuário</strong></td>
										<td><strong>Ação</strong></td>
										<td><strong>Horário</strong></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Gil Vieira</td>
										<td>Alterou dados do projeto</td>
										<td>5 dias atrás</td>
									</tr>
									<tr>
										<td>Gil Vieira</td>
										<td>Alterou dados do projeto</td>
										<td>5 dias atrás</td>
									</tr>
									<tr>
										<td>Gil Vieira</td>
										<td>Alterou dados do projeto</td>
										<td>5 dias atrás</td>
									</tr>
									<tr>
										<td>Gil Vieira</td>
										<td>Alterou dados do projeto</td>
										<td>5 dias atrás</td>
									</tr>
									<tr>
										<td>Gil Vieira</td>
										<td>Alterou dados do projeto</td>
										<td>5 dias atrás</td>
									</tr>
									<tr>
										<td>Gil Vieira</td>
										<td>Alterou dados do projeto</td>
										<td>5 dias atrás</td>
									</tr>
									<tr>
										<td>Gil Vieira</td>
										<td>Alterou dados do projeto</td>
										<td>5 dias atrás</td>
									</tr>
									<tr>
										<td>Gil Vieira</td>
										<td>Alterou dados do projeto</td>
										<td>5 dias atrás</td>
									</tr>
									<tr>
										<td>Gil Vieira</td>
										<td>Alterou dados do projeto</td>
										<td>5 dias atrás</td>
									</tr>
									<tr>
										<td>Gil Vieira</td>
										<td>Alterou dados do projeto</td>
										<td>5 dias atrás</td>
									</tr>
									<tr>
										<td>Gil Vieira</td>
										<td>Alterou dados do projeto</td>
										<td>5 dias atrás</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div><!--/.row-->
