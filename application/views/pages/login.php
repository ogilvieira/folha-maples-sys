<div class="login-wrap">
		<div class="login-container">

			<div class="login-panel panel panel-default">
				<div class="panel-heading">Login</div>
				<div class="panel-body">
					<?php if(validation_errors()): ?>
						<div class="alert alert-danger" role="alert">
							<?php echo validation_errors(); ?>
						</div>
					<?php endif; ?>
					<form role="form" method="post" action="<?php echo base_url('login'); ?>" accept-charset="utf-8">
						<fieldset>
							<div class="form-group">
								<input class="form-control input-lg" placeholder="E-mail" name="email" type="email" autofocus="" required>
							</div>
							<div class="form-group">
								<input class="form-control input-lg" placeholder="Senha" name="password" type="password" value="" required>
							</div>
							<button type="submit" class="btn btn-lg btn-block btn-success">ENTRAR</button>
						</fieldset>
					</form>
				</div>
			</div>

		</div><!-- /.col-->
</div>
<!-- /.login -->