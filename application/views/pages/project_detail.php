<div class="container-fluid">
	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="pull-right"><a href="<?php echo base_url('/project/edit/'.$project['id']); ?>" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-pencil"></span> Editar</a></span>
				<h3 class="panel-title">Básico</h3>
			</div>
			<div class="panel-body">
				<table class="table table-bordered table-responsive table-striped">
					<tr>
						<td><strong>Nome do Projeto:</strong></td>
						<td><?php echo (isset($project['name'])) ? $project['name'] : ''; ?></td>
					</tr>
					<tr>
						<td><strong>Tipo:</strong></td>
						<td>
						<a href="<?php echo base_url('/project?&t='.$project['type']); ?>" target="_blank">
						<?php switch ($project['type']) {
							case '0':
								echo "Web";
								break;
							case '1':
								echo "Marketing";
								break;
							case '2':
								echo "Design";
								break;
							default:
								echo "Outro";
								break;
						}
						?> <i class="glyphicon glyphicon-new-window"></i></a>
						</td>
					</tr>
					<tr>
						<td><strong>Cliente:</strong></td>
						<td>
							<a href="<?php echo base_url('/customer/detail/'.$project['id_customer']); ?>" target="_blank"><?php echo $this->CustomerModel->get($project['id_customer'])['name']; ?> <i class="glyphicon glyphicon-new-window"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>Observações:</strong><br>
							<?php if($project['obs']): ?>
							<?php echo "<code>" . $project['obs'] . "</code>"; ?>
							<?php else: ?>
							<p>
								Nenhuma observação.
							</p>
							<?php endif; ?>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="pull-right"><a href="<?php echo base_url('/project/edit/'.$project['id']); ?>" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-pencil"></span> Editar</a></span>
				<h3 class="panel-title">Produção</h3>
			</div>
			<div class="panel-body">
				<table class="table table-bordered table-responsive table-striped">
					<tr>
						<td><strong>Estágio:</strong></td>
						<td>
							<?php switch ($project['stage']) {
								case '0':
									echo "<span class=\"label label-info\">Pré produção</span>";
									break;
								case '1':
									echo "<span class=\"label label-success\">Produção</span>";
									break;
								case '2':
									echo "<span class=\"label label-info\">Pós produção</span>";
									break;
								default:
									echo "<span class=\"label label-danger\">Finalizado</span>";
									break;
							}
							?>
						</td>
					</tr>
					<tr>
						<td><strong>Data de Entrada:</strong></td>
						<td><?php echo date("d/m/Y", $project['startdate']); ?></td>
					</tr>
					<tr>
						<td><strong>Deadline:</strong></td>
						<td>
							<?php echo date("d/m/Y", $project['deadline']); ?>
						</td>
					</tr>
					<tr>
						<td><strong>Horas Contratadas:</strong></td>
						<td>
							<?php echo $project['hours']; ?>hs
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="pull-right"><a href="<?php echo base_url('/project/edit/'.$project['id']); ?>" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-pencil"></span> Editar</a></span>
				<h3 class="panel-title">Delegar</h3>
			</div>
			<div class="panel-body">
				<table class="table table-bordered table-responsive table-striped">
					<tr>
						<td><strong>Profissional:</strong></td>
						<td>
							<a href="<?php echo base_url('/user/detail/'.$project['id_user']); ?>" target="_blank"><?php echo $this->UserModel->get($project['id_user'])['name']; ?> <i class="glyphicon glyphicon-new-window"></i></a>
						</td>
					</tr>
					<tr>
						<td><strong>Visível no Pipeline:</strong></td>
						<td><?php echo $project['pipeline'] ? 'Sim' : 'Não'; ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	
	<div class="col-lg-12">
		<hr>
	</div>

	<div class="col-xs-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Log de atividades</h3>
			</div>
			<div class="panel-body">
				<table class="table table-bordered table-responsive table-striped">
					<tr>
						<td><strong>Profissional:</strong></td>
						<td>
							<a href="<?php echo base_url('/user/detail/'.$project['id_user']); ?>" target="_blank"><?php echo $this->UserModel->get($project['id_user'])['name']; ?> <i class="glyphicon glyphicon-new-window"></i></a>
						</td>
					</tr>
					<tr>
						<td><strong>Visível no Pipeline:</strong></td>
						<td><?php echo $project['pipeline'] ? 'Sim' : 'Não'; ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
	
