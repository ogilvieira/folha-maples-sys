<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading"></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-6">
						<form action="" id="passwordgen">
						<div class="form-group">

							<label for="">Login/Nome de Usuário</label>
							<input name="username" class="form-control" type="text" required>
						</div>
						<div class="form-group">
							<label for="">Quantidade de Caracteres:</label>
							<select name="charlen" class="form-control" id="" required>
								<option value="6">6</option>
								<option value="8" selected>8</option>
								<option value="10">10</option>
								<option value="12">12</option>
							</select>

						</div>
						<div class="form-group">
							<label for="">Versão:</label>
							<select name="ver" class="form-control" id="" required>
								<option value="maples">Versão 1</option>
								<option value="tica">Versão 2</option>
							</select>
						</div>
						<button type="submit" class="btn btn-lg btn-block btn-primary"><i class="fa fa-shopping-cart"></i> GERAR SENHA</button>
						<br>
						<br>
						</form>
					</div>
					<div class="col-lg-6">
						<div class="input-group input-group-lg">
							<input id="passresult" class="form-control" placeholder="A senha vai aparecer aqui">
							<div class="input-group-btn">
								<button type="button" data-clipboard-target="#passresult" class="btn btn-lg btn-success btn-copy-result">Copiar</button>
							</div>
						</div>
						<hr>
						<p>
							Ao criar um usuário usar o seguinte padrão de Username:<br>
							<strong class="label label-danger">seunome@nomedocliente</strong>
							<br>
							<br>
							Para usuário coletivo da Maples:<br>
							<strong class="label label-danger">maples@nomedocliente</strong>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!--/.row-->
