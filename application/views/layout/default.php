<!DOCTYPE html>
<html class="no-js" lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?php echo $this->title; ?></title>
  <meta name="viewport" content="width=device-witdh, initial-scale=1">

  <!-- favicons -->
  <link rel="shortcut icon" href="<?php echo base_url('favicon.png'); ?>">
  <link rel="apple-touch-icon" href="">

  <!-- stylesheets:app -->
  <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/datepicker3.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/bootstrap-select.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/app.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

<script>
  var SITEURL = "<?php echo base_url(); ?>";
</script>

</head>
<body class="<?php echo $this->body_classes; ?>">
<?php if($this->session_data): ?>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url(); ?>"><span>Folha</span>Sys</a>
        </div>
        <div class="collapse navbar-collapse" id="sidebar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li <?php echo ($this->menu_target == "dashboard") ? "class=\"active\"" : "" ?>>
              <a href="<?php echo base_url(); ?>"><i class="glyphicon glyphicon-dashboard"></i> Dashboard</a>

            </li>
            <li <?php echo ($this->menu_target == "project") ? "class=\"active dropdown\"" : "class=\"dropdown\"" ?>>
              <a href="<?php echo base_url('project'); ?>" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-briefcase"></i> Projetos <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url('project'); ?>">Ver todos</a></li>
                <li><a href="<?php echo base_url('project/add'); ?>">Novo Projeto</a></li>
              </ul>
            </li>
            <li <?php echo ($this->menu_target == "password") ? "class=\"active\"" : "" ?>>
              <a href="<?php echo base_url('password'); ?>"><i class="glyphicon glyphicon-lock"></i> Gerador de senhas</a>

            </li>
            <li <?php echo ($this->menu_target == "user") ? "class=\"dropdown\"" : "dropdown" ?>>
              <a href="<?php echo base_url('user'); ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i> Usuários <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url('user'); ?>">Ver todos</a></li>
                <li><a href="<?php echo base_url('user/add'); ?>">Novo Usuário</a></li>
              </ul>
            </li>
            <li <?php echo ($this->menu_target == "customer") ? "class=\"dropdown\"" : "dropdown" ?>>
              <a href="<?php echo base_url('customer'); ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-heart"></i> Clientes <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url('customer'); ?>">Ver todos</a></li>
                <li><a href="<?php echo base_url('customer/add'); ?>">Novo Cliente</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="<?php echo base_url('profile'); ?>" class="<?php echo ($this->menu_target == "me") ? "class=\"dropdown\"" : "dropdown" ?>" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> Minha Conta<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="<?php echo base_url('profile'); ?>"><i class="glyphicon glyphicon-user"></i> Perfil</a></li>
                  <li><a href="<?php echo base_url('profile/setup'); ?>"><i class="glyphicon glyphicon-cog"></i> Configurações</a></li>
                  <li><a href="<?php echo base_url('logout'); ?>"><i class="glyphicon glyphicon-log-out"></i> Sair</a></li>
                </ul>
              </li>
          </ul>
        </div>
      </div><!-- /.container-fluid -->
    </nav>

    <main>
      <?php if(isset($this->page_title)): ?>
        <div class="container-fluid">
          <div class="col-lg-12">
            <div class="page-header">
              <h1><?php echo $this->page_title; ?></h1>
            </div>
          </div>
          <div class="col-lg-12">
            <?php echo $this->breadcrumbs->show(); ?>            
          </div>
        </div>
      <?php endif; ?>
      <?php
      if( $this->session->flashdata() ):
        echo "<div class=\"container-fluid\"><div class=\"col-lg-12\">";
        $this->load->view('partial/alert');
        echo "</div></div>";
      endif;
      ?>
    <?php endif; ?>
    <?php echo $page_content; ?>
    </main>

    <footer>
      <div class="container-fluid">
        <div class="col-lg-12">
          <center>
            <hr>
            <p> Desenvolvido por<br><strong>Maples</strong></p>
          </center>
        </div>
      </div>
    </footer>

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chart.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chart-data.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/easypiechart.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/easypiechart-data.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datepicker-pt-BR.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-table.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/clipboard.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.mask.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/passwordGen.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/app.js"></script>
  </body>
</html>