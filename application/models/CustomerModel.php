<?php
Class CustomerModel extends CI_Model
{
  function setTimeStamp($i)
  {
    $o = new DateTime(str_replace("/", "-", $i));
    $o = strtotime($o->format('Y-m-d'));
    return $o;
  }

 function get($id=false, $limit=false, $page=1, $search=false, $orderby='id', $order='ASC')
 {
  if($limit):
  $offset = (($page-1 < 0) ? 0 : $page-1)*$limit;
  else:
  $offset = 0;
  endif;

   $this->db->select('*');
   $this->db->from('customers');
   $this->db->order_by($orderby, $order);
   if($search):
    foreach($search as $key => $argument ):
      if(strlen($argument)):
        if($key == 'hide3'){
          $this->db->where('stage !=', 3);
        } else {
          $this->db->like($key, $argument);
        }
      endif;
    endforeach;
  endif;
   if($id):
     $this->db->where('id', $id);
   else:
    if($limit):
      $this->db->limit($limit, $offset);
    endif;
   endif;

    $query = $this->db->get();
    $result = $query->result();

   if($query->num_rows())
   {
    if($id):
     return json_decode(json_encode($result[0]), true);
    endif;


    $pagination = array();

    if($limit):
      $pagination['items'] = $query->num_rows();
      $pagination['page_active'] = $page;
      $pagination['page_total'] = round($pagination['items']/$limit)+1;
      $pagination['page_limit'] = $limit;
    endif;

    return array("result" => $result, "pagination" => $pagination);
   }
   else
   {
     return false;
   }
 }

 function add($params)
 {

  if(!isset($params['name']) || empty($params['name'])){
    $this->session->set_flashdata('danger_message', 'O Nome do cliente é requerido.');
    return false;
  };

  $this->db->select('name');
  $this->db->from('customers');
  $this->db->where('name', $params['name']);
  $this->db->limit(1);
  $query = $this->db->get();
  
  if($query->num_rows()) 
  { 
    $this->session->set_flashdata('danger_message', 'Já existe um cliente com este nome.');
    return false;
  } else {
    $params['create_in'] = time();
    $this->db->insert('customers', $params);
    $this->session->set_flashdata('success_message', 'O cliente foi cadastrado.');
    return true;
  }
 }

 function update($id, $params)
 {
  if(!$params || !$id){
    $this->session->set_flashdata('danger_message', 'Erro ao enviar dados, tente novamente.');
    return false;
  };

  if(!isset($params['name']) || empty($params['name'])){ 
    $this->session->set_flashdata('danger_message', 'O Nome do cliente é requerido.');
    return false;
  };

  if($params['date_plan']):
    if($params['date_plan'] == "00/00/0000"){
      $params['date_plan'] = '';
    } else {
      $params['date_plan'] = $this->setTimeStamp($params['date_plan']);
    }
  endif;

  $this->db->where('id', $id);
  $this->db->update('customers', $params);
  $this->session->set_flashdata('success_message', 'Cliente atualizado.');
  return true;
 }
}
?>