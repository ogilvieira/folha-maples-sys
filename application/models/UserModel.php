<?php
Class UserModel extends CI_Model
{
 function get($id=false, $limit=false, $page=1, $search=false)
 {
  if(!$limit){ $limit = 10; };
  $offset = (($page-1 < 0) ? 0 : $page-1)*$limit;

   $this->db->select('*');
   $this->db->from('users');
   if($search):
     $this->db->like($search['col'], $search['value']);
  endif;
   if($id):
     $this->db->where('id', $id);
   else:
    $this->db->limit($limit, $offset);
   endif;


    $query = $this->db->get();
    $result = $query->result();

   if($query->num_rows())
   {
    if($id):
     return json_decode(json_encode($result[0]), true);
    endif;


    $pagination = array();
    $pagination['items'] = $this->db->count_all('users');
    $pagination['page_active'] = $page;
    $pagination['page_total'] = round($pagination['items']/$limit)+1;
    $pagination['page_limit'] = $limit;

    return array("result" => $result, "pagination" => $pagination);
   }
   else
   {
     return false;
   }
 }

 function add($params)
 {
  if(!$params){
    $this->session->set_flashdata('danger_message', 'Erro ao enviar dados, tente novamente.');
    return false;
  };

  if(!isset($params['email']) || empty($params['email'])){ 
    $this->session->set_flashdata('danger_message', 'O e-mail é requerido.');
    return false;
  };

  if(!isset($params['name']) || empty($params['name'])){ 
    $this->session->set_flashdata('danger_message', 'O Nome é requerido.');
    return false;
  };

  if(!isset($params['password']) || empty($params['password'])){
    $this->session->set_flashdata('danger_message', 'A senha é requerida.');
    return false;
  }
  if(!$params['password'] !== !$params['password_compare']){ 
    $this->session->set_flashdata('danger_message', 'As senhas devem ser iguais');
    return false;
  };

  $this->db->select('email');
  $this->db->from('users');
  $this->db->where('email', $params['email']);
  $this->db->limit(1);
  $query = $this->db->get();
  
  if($query->num_rows()) 
  { 
    $this->session->set_flashdata('danger_message', 'Já existe um usuário com este e-mail.');
    return false;
  } else {
    unset($params['password_compare']);
    $params['password'] = MD5($params['password']);
    $params['create_in'] = time();
    $this->db->insert('users', $params);
    $this->session->set_flashdata('success_message', 'O usuário foi cadastrado.');
    return true;
  }
 }

 function update($id, $params)
 {
  if(!$params || !$id){
    $this->session->set_flashdata('danger_message', 'Erro ao enviar dados, tente novamente.');
    return false;
  };

  if(!isset($params['email']) || empty($params['email'])){ 
    $this->session->set_flashdata('danger_message', 'O e-mail é requerido.');
    return false;
  };

  if(!isset($params['name']) || empty($params['name'])){ 
    $this->session->set_flashdata('danger_message', 'O Nome é requerido.');
    return false;
  };


  if(isset($params['password']) && !empty($params['password'])):
    if($params['password'] !== $params['password_compare']){ 
      $this->session->set_flashdata('danger_message', 'As senhas devem ser iguais');
      return false;
    };
  endif;


  if(empty($params['password'])):
    unset($params['password']);
  else:
    $params['password'] = MD5($params['password']);
  endif;
  unset($params['password_compare']);

  $this->db->where('id', $id);
  $this->db->update('users', $params);
  $this->session->set_flashdata('success_message', 'Cadastro atualizado.');
  return true;
 }

 function getSatus($id)
 {
   
   $this->db->select('id, status');
   $this->db->from('users');
   $this->db->where('id', $id);
   $this->db->limit(1);
 
   $query= $this->db->get();

   if($query -> num_rows() == 1)
   {
    $res = $query->result();
     return $res[0]->status;
   }
   else
   {
     return false;
   }
 } 
 function login($email, $password)
 {
   
   $this->db->select('id, password, name, email, status');
   $this->db->from('users');
   $this->db->where('email', $email);
   $this->db->where('password', MD5($password));
   $this->db->where('status', 1);
   $this->db->limit(1);
 
   $query= $this->db->get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
}
?>