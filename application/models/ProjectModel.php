<?php
Class ProjectModel extends CI_Model
{

  function setTimeStamp($i)
  {
    $o = new DateTime(str_replace("/", "-", $i));
    $o = strtotime($o->format('Y-m-d'));
    return $o;
  }

 function get($id=false, $limit=false, $page=1, $search=false, $orderby='id', $order='ASC')
 {
  if($limit):
  $offset = (($page-1 < 0) ? 0 : $page-1)*$limit;
  else:
  $offset = 0;
  endif;

   $this->db->select('*');
   $this->db->from('projects');
   $this->db->order_by($orderby, $order);
   if($search):
    foreach($search as $key => $argument ):
      if(strlen($argument)):
        if($key == 'hide3'){
          $this->db->where('stage !=', 3);
        } else {
          $this->db->like($key, $argument);
        }
      endif;
    endforeach;
  endif;
   if($id):
     $this->db->where('id', $id);
   else:
    if($limit):
      $this->db->limit($limit, $offset);
    endif;
   endif;

  $query = $this->db->get();
  $result = $query->result();

   if($query->num_rows())
   {
    if($id):
     return json_decode(json_encode($result[0]), true);
    endif;


    $pagination = array();

    if($limit):
      $pagination['items'] = $this->db->count_all('projects');
      $pagination['page_active'] = $page;
      $pagination['page_total'] = round($pagination['items']/$limit)+1;
      $pagination['page_limit'] = $limit;
    endif;

    return array("result" => $result, "pagination" => $pagination);
   }
   else
   {
     return false;
   }
 }

 function add($params)
 {

  if(!isset($params['name']) || empty($params['name'])){
    $this->session->set_flashdata('danger_message', 'O Nome do projeto é requerido.');
    return false;
  };

  if(!isset($params['deadline']) || empty($params['deadline'])){
    $this->session->set_flashdata('danger_message', 'A Deadline do projeto é requerida.');
    return false;
  };

  if(!isset($params['startdate']) || empty($params['startdate'])){
    $this->session->set_flashdata('danger_message', 'A Data de inicio do projeto é requerida.');
    return false;
  };


  if($params['deadline'] == "00/00/0000"){
    $params['deadline'] = time();
  } else {
    $params['deadline'] = $this->setTimeStamp($params['deadline']);
  }
  if($params['startdate'] == "00/00/0000"){
    $params['startdate'] = time();
  } else {
    $params['startdate'] = $this->setTimeStamp($params['startdate']);
  }

  $params['hours'] = str_replace('hs', '', $params['hours']);

  $params['create_in'] = time();
  $this->db->insert('projects', $params);
  $this->session->set_flashdata('success_message', 'O projeto foi cadastrado.');

  $r = array(
    'err' => 0,
    'insert_id' => $this->db->insert_id()
  );

  return $r;
 }

 function update($id, $params)
 {
  if(!isset($params['name']) || empty($params['name'])){
    $this->session->set_flashdata('danger_message', 'O Nome do projeto é requerido.');
    return false;
  };

  if(!isset($params['deadline']) || empty($params['deadline'])){
    $this->session->set_flashdata('danger_message', 'A Deadline do projeto é requerida.');
    return false;
  };

  if(!isset($params['startdate']) || empty($params['startdate'])){
    $this->session->set_flashdata('danger_message', 'A data de inicio do projeto é requerida.');
    return false;
  };

  if($params['deadline'] == "00/00/0000"){
    $params['deadline'] = time();
  } else {
    $params['deadline'] = $this->setTimeStamp($params['deadline']);
  }
  if($params['startdate'] == "00/00/0000"){
    $params['startdate'] = time();
  } else {
    $params['startdate'] = $this->setTimeStamp($params['startdate']);
  }

  $params['hours'] = str_replace('hs', '', $params['hours']);

  $this->db->where('id', $id);
  $this->db->update('projects', $params);
  $this->session->set_flashdata('success_message', 'Projeto atualizado.');
  return true;
 }
}
?>