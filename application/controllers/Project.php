<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MPLS_Controller.php';

class Project extends MPLS_Controller {
	function __construct(){
		parent::__construct();
		$this->menu_target = "project";
		$this->page_title = "Projetos";
		$this->load->model('UserModel','',TRUE);
		$this->load->model('CustomerModel','',TRUE);
		$this->load->model('ProjectModel','',TRUE);
		$this->breadcrumbs->push('Projetos', '/project');
	}
	public function index()
	{
		$data = array();
		

		$page = intval(isset($_GET['p']) ? $_GET['p'] : 1);
		$limit = intval(isset($_GET['l']) ? $_GET['l'] : 12);

		$data['query'] = array();
		$data['query']['name'] = isset($_GET['n']) ? $_GET['n'] : '';
		$data['query']['type'] = isset($_GET['t']) ? $_GET['t'] : '';
		$data['query']['id_user'] = isset($_GET['r']) ? $_GET['r'] : '';
		$data['query']['stage'] = (isset($_GET['s']) ? (intval($_GET['s']) > 3 ? 3 : $_GET['s']) : '');
		$data['query']['hide3'] = isset($_GET['hide3']) ? $_GET['hide3'] : '';
		$data['query']['id_customer'] = (isset($_GET['c']) ? $_GET['c'] : false);
		$data['query']['service_name'] = 'project';

		$data['customers'] = $this->CustomerModel->get(false, false, 1, false, 'name', 'asc');
		$data['customers'] = $data['customers']['result'];
		$dbresult = $this->ProjectModel->get(false, $limit, $page, $data['query'], 'name', 'asc');


		if($dbresult){
			$data['projects'] = $dbresult['result'];
			$data['pagination'] = $dbresult['pagination'];
		} else {
			$data['projects'] = false;
			$data['pagination'] = false;
		}

		$this->view('pages/project_list', $data);
	}
	public function add()
	{
		$this->page_title = "Novo Projeto";
		$this->breadcrumbs->push('Novo Projeto', '/project/add');

		$data = array();
		$data['projectData'] = array();
		$data['customers'] = $this->CustomerModel->get(false, false, 1, false, 'name', 'asc');
		$data['customers'] = $data['customers']['result'];
		$data['users'] = $this->UserModel->get();
		$data['users'] = $data['users']['result'];

		if($this->input->post()):
			$data['projectData'] = $this->input->post();

			$result = $this->ProjectModel->add($data['projectData']);

			if(isset($result)){
				redirect('project/detail/'.$result['insert_id'], 'refresh');
			}

		endif;

		$this->view('pages/project_add', $data);
	}

	public function edit($id=0)
	{
		$this->page_title = "Editar Projeto";

		$data = array();
		$data['projectData'] = array();
		$data['customers'] = $this->CustomerModel->get(false, false, 1, false, 'name', 'asc');
		$data['customers'] = $data['customers']['result'];
		$data['users'] = $this->UserModel->get();
		$data['users'] = $data['users']['result'];

		if($id == 0):
			$this->session->set_flashdata('danger_message', 'Parâmetros incorretos.');
			redirect('project', 'refresh');
		else:
			$data['projectData'] = $this->ProjectModel->get($id);
			if(!$data['projectData']){
				$this->session->set_flashdata('danger_message', 'Parâmetros incorretos.');
				redirect('project', 'refresh');
			}
		endif;

		if($this->input->post()):
			$data['projectData'] = $this->input->post();

			if($this->input->post()):
				$result = $this->ProjectModel->update($id, $this->input->post());
				if($result){
					redirect('project/detail/'.$id, 'refresh');
				} else {
					redirect('project/edit/'.$id, 'refresh');
				}
			endif;
		endif;
		
		$this->breadcrumbs->push($data['projectData']['name'], '/project/detail/' . $data['projectData']['id']);
		$this->breadcrumbs->push('Editar', '/project/edit');

		$this->view('pages/project_edit', $data);
	}

	public function detail($id=0)
	{
		$data = array();
		$data['project'] = $this->ProjectModel->get($id, 1, 1, array('service_name' => 'project'), 'name', 'asc');
		if(!$data['project']){
			$this->session->set_flashdata('danger_message', 'Parâmetros incorretos.');
			redirect('project', 'refresh');
		}
		$this->page_title = "#" . $data['project']['id'] ." - ". $data['project']['name'];
		$this->breadcrumbs->push($data['project']['name'], '/project/detail');

		$this->view('pages/project_detail', $data);
	}

}
