<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MPLS_Controller.php';

class Customer extends MPLS_Controller {
	function __construct(){
		parent::__construct();
		$this->menu_target = "customer";
		$this->page_title = "Clientes";
		$this->load->model('CustomerModel','',TRUE);
		$this->load->model('ProjectModel','',TRUE);
		$this->breadcrumbs->push('Clientes', '/customer');
	}
	public function index()
	{
		$data = array();

		$data['query'] = array();
		$data['query']['name'] = isset($_GET['n']) ? $_GET['n'] : '';
		$data['query']['monthly'] = isset($_GET['m']) ? $_GET['m'] : '';

		$page = intval(isset($_GET['p']) ? $_GET['p'] : 1);
		$limit = intval(isset($_GET['l']) ? $_GET['l'] : 10);
		$dbresult = $this->CustomerModel->get(false, $limit, $page, $data['query'], 'name');

		if($dbresult):
			$data['customers'] = $dbresult['result'];
			$data['pagination'] = $dbresult['pagination'];
		else:
			$data['customers'] = false;
			$data['pagination'] = false;
		endif;

		$this->view('pages/customer_list', $data);
	}
	public function add()
	{
		$data = array();
		$data['customerData'] = array();
		$this->page_title = "Adicionar Cliente";
		$this->breadcrumbs->push('Adicionar Cliente', '/customer/add');

		if($this->input->post()):
			$data['customerData'] = $this->input->post();
			$result = $this->CustomerModel->add($data['customerData']);
			if($result){
				redirect('customer/detail/'.$id, 'refresh');
			} else {
				redirect('customer', 'refresh');
			}
		endif;

		$this->view('pages/customer_add', $data);
	}
	public function edit($id=0)
	{
		$data = array();
		$data['customerData'] = array();
		$this->page_title = "Editar Cliente";

		if($id == 0):
			$this->session->set_flashdata('danger_message', 'Parâmetros incorretos.');
			redirect('customer', 'refresh');
		else:
			$data['customerData'] = $this->CustomerModel->get($id);
		endif;


		if($this->input->post()):
			$result = $this->CustomerModel->update($id, $this->input->post());
			if($result){
				redirect('customer/detail/'.$id, 'refresh');
			} else {
				redirect('customers/edit/'.$id, 'refresh');
			}
		endif;

		$this->breadcrumbs->push($data['customerData']['name'], '/customer/detail/' . $data['customerData']['id']);
		$this->breadcrumbs->push('Editar Cliente', '/customer/edit');
		$this->view('pages/customer_edit', $data);
	}
	public function detail($id=0)
	{
		$data = array();
		$data['customer'] = $this->CustomerModel->get($id, 1, 1, false, 'name', 'asc');
		$data['projects'] = $this->ProjectModel->get(false, 3, false, array( 'id_customer' => $id ), 'name', 'asc')['result'];

		$this->page_title = $data['customer']['name'];
		$this->breadcrumbs->push($data['customer']['name'], '/customer/detail');

		$this->view('pages/customer_detail', $data);
	}
}
