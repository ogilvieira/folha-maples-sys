<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MPLS_Controller.php';

class Profile extends MPLS_Controller {
	function __construct(){
		parent::__construct();
		$this->menu_target = "me";
	}
	public function index()
	{
		$data = array();
		
		$id = $this->session->userdata('logged_in')["id"];
		$data['profileData'] = $this->UserModel->get($id);

		$this->page_title = $data['profileData']['name'];
		$this->breadcrumbs->push($data['profileData']['name'], '/profile');

		$this->view('pages/profile', $data);
	}
	public function setup()
	{
		$data = array();
		
		$id = $this->session->userdata('logged_in')["id"];
		$data['profileData'] = $this->UserModel->get($id);

		$this->page_title = "Editar conta";
		$this->breadcrumbs->push($data['profileData']['name'], '/profile');
		$this->breadcrumbs->push("Editar", '/profile/setup');

		$this->view('pages/profile', $data);
	}
}

