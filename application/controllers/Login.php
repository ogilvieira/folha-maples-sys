<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MPLS_Controller.php';

class Login extends MPLS_Controller {
	
	function __construct(){
		$this->checkUser = false;
		parent::__construct();
	}
	public function index()
	{
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Senha', 'trim|required|xss_clean|callback_check_database');

		if($this->form_validation->run() == FALSE)
		{
		 $this->view('pages/login');
		}
		else
		{
		 redirect('dashboard', 'refresh');
		}
	}

	 function check_database($password)
	 {
	   //Field validation succeeded.  Validate against database
	   $email = $this->input->post('email');	 
	   //query the database
	   $result = $this->UserModel->login($email, $password);
	 
	   if($result)
	   {
	     $sess_array = array();
	     foreach($result as $row)
	     {
	       $sess_array = array(
	         'id' => $row->id,
	         'name' => $row->name,
	         'email' => $row->email,
	         'status' => $row->status,
	       );
	       $this->session->set_userdata('logged_in', $sess_array);
	     }
	     return TRUE;
	   }
	   else
	   {
	     $this->form_validation->set_message('check_database', 'Senha ou usuário inválido');
	     return false;
	   }
	 } 
}
