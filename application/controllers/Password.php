<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MPLS_Controller.php';

class Password extends MPLS_Controller {
	function __construct(){
		parent::__construct();
		$this->page_title = "Gerador de Senhas";
    $this->menu_target = "password";
    $this->breadcrumbs->push('Gerador de Senhas', '/password');
	}

  public function index()
  {
    $this->view('pages/password');
  }

	public function get()
	{
    header('Content-Type: application/json');
    $arr = array();

    if($_SERVER['REQUEST_METHOD'] !== 'POST'):
      $arr = array('err' => 1, 'msg' => 'Invalid request');
      echo json_encode($arr);
      return;
    endif;

    function random_password($username = '', $version = 'maples', $length = 8 ) {
        // Versões:
        // maples
        // tica
        $password = substr( sha1($username.$version), 0, $length);
        return $password;
    };

    $passData = $this->input->post();

    $arr['err'] = 0;
    $arr['msg'] = random_password($passData['username'], $passData['ver'], $passData['charlen']);

    echo json_encode($arr);
	}
}
