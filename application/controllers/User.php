<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MPLS_Controller.php';

class User extends MPLS_Controller {
	function __construct(){
		parent::__construct();
		$this->menu_target = "user";
		$this->page_title = "Usuários";
		$this->breadcrumbs->push('Usuários', '/user');
	}
	public function index()
	{
		$data = array();
		$page = intval(isset($_GET['p']) ? $_GET['p'] : 1);
		$limit = intval(isset($_GET['l']) ? $_GET['l'] : 10);
		$dbresult = $this->UserModel->get(false, $limit, $page);

		if($dbresult):
			$data['users'] = $dbresult['result'];
			$data['pagination'] = $dbresult['pagination'];
		else:
			$data['users'] = false;
			$data['pagination'] = false;
		endif;

		$this->view('pages/user_list', $data);
	}
	public function add()
	{
		$data = array();
		$data['userData'] = array();

		$this->page_title = "Adicionar Usuário";
		$this->breadcrumbs->push('Adicionar Usuário', '/project/add');

		if($this->input->post()):
			$data['userData'] = $this->input->post();
			$result = $this->UserModel->add($data['userData']);
			if($result){
				redirect('user', 'refresh');
			}
		endif;

		$this->view('pages/user_add', $data);
	}
	public function edit($id=0)
	{
		$data = array();
		$data['userData'] = array();

		$this->page_title = "Editar Usuário";
		$this->breadcrumbs->push('Editar Usuário', '/project/edit');

		if($id == 0):
			$this->session->set_flashdata('danger_message', 'Parâmetros incorretos.');
			redirect('user', 'refresh');
		else:
			$data['userData'] = $this->UserModel->get($id);
		endif;

		if($this->input->post()):
			$result = $this->UserModel->update($id, $this->input->post());
			if($result){
				redirect('user', 'refresh');
			} else {
				redirect('user/edit/'.$id, 'refresh');
			}
		endif;

		$this->view('pages/user_edit', $data);
	}
}

