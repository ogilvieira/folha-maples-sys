<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MPLS_Controller.php';

class Dashboard extends MPLS_Controller {
	function __construct(){
		parent::__construct();
		$this->page_title = "Dashboard";
    $this->load->model('UserModel','',TRUE);
    $this->load->model('CustomerModel','',TRUE);
    $this->load->model('ProjectModel','',TRUE);
    $this->breadcrumbs->push('Dashboard', '/dashboard');
	}
		

	public function index()
	{
    $data = array();

    $data['userId'] = $this->session->userdata('logged_in')["id"];

    $data['query'] = array(
      "id_user" => $data['userId'],
      "pipeline" => 1,
      "hide3" => 1,
    );
    $data['taskboard'] = $this->ProjectModel->get(false, 5, 0, $data['query'], 'name', 'asc')['result'];

		$this->view('pages/dashboard', $data);
	}
}
