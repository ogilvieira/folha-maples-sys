<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MPLS_Controller.php';

class Logout extends MPLS_Controller {
	function __construct(){
		$this->checkUser = false;
		parent::__construct();
	}
	public function index()
	 {
	   $this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect('login', 'refresh');
	 }
}
