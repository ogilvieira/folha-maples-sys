<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/MPLS_Controller.php';

class CustomerPassword extends MPLS_Controller {
	function __construct(){
		parent::__construct();
		$this->page_title = "Senhas Salvas";
    $this->menu_target = "customerpassword";
	}

  public function index()
  {
    $this->view('pages/customerpassword');
  }
}
